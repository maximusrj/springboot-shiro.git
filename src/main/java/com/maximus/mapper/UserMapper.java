package com.maximus.mapper;

import com.maximus.domain.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @Auther:
 * @Date:
 * @Description:
 */
public interface UserMapper {
     User findByName(@Param("name") String name);
     User findById(@Param("id") Integer id);
}
