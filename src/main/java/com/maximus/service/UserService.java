package com.maximus.service;

import com.maximus.domain.User;

/**
 * @Auther:
 * @Date:
 * @Description:
 */
public interface UserService {
     User findByName(String name);
     User findById(Integer id);
}
